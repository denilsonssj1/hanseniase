# Descoberta em bases de dados sobre a hanseníase no estado do Tocantins

## 1 - Pré-Processamento de Dados

Entre no link abaixo para visualizar o resultado do arquivo [preprocessing.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/denilsonssj1/hanseniase/-/raw/master/preprocessing.ipynb?flush_cache=true)

## 2 - Análise Exploratória de Dados

Entre no link abaixo para visualizar o resultado do arquivo [exploratory_analysis.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/denilsonssj1/hanseniase/-/raw/master/exploratory_analysis.ipynb?flush_cache=true)

## 3 - Mineração de Dados

Entre no link abaixo para visualizar o resultado do arquivo [data_mining.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/denilsonssj1/hanseniase/-/raw/master/data_mining.ipynb?flush_cache=true)

## 4 - Visualização de Dados

Entre no link abaixo para visualizar o resultado do arquivo [visualization.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/denilsonssj1/hanseniase/-/raw/master/visualization.ipynb?flush_cache=true)